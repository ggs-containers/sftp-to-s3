source .env
# export RUST_LOG=debug
# export RUST_LOG=info
export RUST_LOG=sftp_to_s3=info
export RUST_BACKTRACE=1
cargo run --release --bin sftp_to_s3 -- \
    --sftp-source-path "${SFTP_SOURCE_PATH}" \
    --sftp-get-option "get" \
    --sftp-filter-regex "_\\d{2}$|fail_to_download|subdir" \
    --bucket-name "${BUCKET_NAME}" \
    --bucket-prefix "${BUCKET_PREFIX}" \
    --sftp-download-mode sftp \
    --sftp-n-sessions 4
