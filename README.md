# sftp-to-s3

Container for performing ingest from SFTP directories into S3 , configured by args and environment variables. Concurrently download files from a SFTP remote directory and concurrently upload to a S3 bucket prefix.

This application is written in Rust at <https://gitlab.com/ggs-containers/sftp-to-s3> .

# How it works

There are three channels and steps performed by async spawned tasks:

0. Create main SFTP session and (AWS S3?) bucket client session. Check AWS STS identity and log it.
1. List SFTP remote directory files and publish into channel A.
2. BLOCK ON 1
3. Spread tasks to download files published into channel A. For every downloaded file, those tasks publish the local path and sftp path into channel B.
4. Spread tasks to upload files to S3. Those tasks consume channel B elements, and perform upload to S3 and deletion of the local temp file downloaded. It publishes into channel C the result OK or KO of the upload process, with error information.
5. BLOCK ON 4 tasks
6. Resume process from channel C published information.
7. Exit 1 if there is a succes rate < 1.0.

# Usage

There are configuration examples on `docker-compose.yaml`, configuration is splitted between args and environment variables.

```
services:
  sftp_to_s3:
    image: extract_to_s3
    command: ./sftp_to_s3 --sftp-source-path "${SFTP_SOURCE_PATH}"
             --sftp-get-option "${SFTP_GET_OPTION}"
             --bucket-name "${BUCKET_NAME}"
             --bucket-prefix "${BUCKET_PREFIX}"
             --bucket-files-acl "bucket-owner-full-control"
             --bucket-files-storage-class "STANDARD"
    environment:
      - SFTP_USER=${SFTP_USER}
      - SFTP_PASSWORD=${SFTP_PASSWORD}
      - SFTP_SSH_PRIVATE_KEY=${SFTP_SSH_PRIVATE_KEY}      
      - SFTP_HOST=${SFTP_HOST}
      - SFTP_PORT=${SFTP_PORT}     
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
      - RUST_BACKTRACE=1
```

## Configuration by args

Behaviour type variables are provided by args.

```
USAGE:
    sftp_to_s3 [OPTIONS] --sftp-source-path <SFTP_SOURCE_PATH> --bucket-name <BUCKET_NAME> --bucket-prefix <BUCKET_PREFIX>

OPTIONS:
        --bucket-files-acl <BUCKET_FILES_ACL>
            target bucket ACL of files copied, a value from
            https://docs.rs/aws-sdk-s3/latest/aws_sdk_s3/model/enum.ObjectCannedAcl.html and
            https://docs.rs/aws-sdk-s3/latest/src/aws_sdk_s3/model.rs.html#3245-3257 [default:
            bucket-owner-full-control]

        --bucket-files-storage-class <BUCKET_FILES_STORAGE_CLASS>
            target bucket storage class of files copied, a value from
            https://docs.rs/aws-sdk-s3/latest/aws_sdk_s3/model/enum.StorageClass.html and
            https://docs.rs/aws-sdk-s3/latest/src/aws_sdk_s3/model.rs.html#36-49 [default: STANDARD]

        --bucket-name <BUCKET_NAME>
            target bucket name

        --bucket-prefix <BUCKET_PREFIX>
            target bucket prefix

        --bucket-region <BUCKET_REGION>
            target bucket region [default: eu-west-1]

    -h, --help
            Print help information

        --local-folder <LOCAL_FOLDER>
            local temporal auxiliar folder where download data [default: /tmp/__sftp_download__]

        --output-json-path <OUTPUT_JSON_PATH>
            summary json output path [default: /tmp/sftp_to_s3_results.json]

        --sftp-filter-regex <SFTP_FILTER_REGEX>
            regex to filter valid processing paths from origin sftp. The path must verify the regex
            to be loaded and transfered. For example: `"_\\d{2}$|fail_to_download|subdir"` [default:
            None]

        --sftp-get-option <SFTP_GET_OPTION>
            wether if delete files from sftp after correct upload to s3, possible values are GET and
            DELETE [default: GET]

        --sftp-diff-option <SFTP_DIFF_OPTION>
          possible values are: - diffgreatequallastmodified (DEFAULT): it only copies files if last modified time on sftp is great or equal than s3 last modified time - diffexisting: it only copies files if file does not exist in s3 - nodiff: it always copies independently of s3 existence or last modified time on target key [default: diffgreatequallastmodified]

        --sftp-download-mode <SFTP_DOWNLOAD_MODE>
            one of those values (if not provided it takes default value): - `sftp`: to perform
            download using sftp client (slower). - `scp`: to perform download using scp (faster but
            not available always). **DEFAULT** - `asyncsftp`: to perform download using async sftp
            session (testing, not really performant) [default: scp]

        --sftp-last-mtime <SFTP_LAST_MTIME>
            date to ingest from in linux timestamp (date +%s) to be compared with file MTIME
            [default: 0]

        --sftp-buffer-size <SFTP_BUFFER_SIZE>
          size of sftp copying buffer in bytes [default: 16777216]

        --sftp-n-sessions <SFTP_N_SESSIONS>
            number of parallel sftp sessions to perform download [default: 3]

        --sftp-source-path <SFTP_SOURCE_PATH>
            path of the sftp remote filesystem to ingest

    -V, --version
            Print version information
```

## Configuration by environment variables

Credential type variables are provided by environment.

### SFTP

* `SFTP_HOST`, host of the sftp server, IP or dns name without protocol prefixes. **REQUIRED**
* `SFTP_PORT`, port for sftp server. **REQUIRED**
* `SFTP_USER`, user for log in sftp server. **REQUIRED**
One of those two options (if both received, `SFTP_SSH_PRIVATE_KEY` takes priority):
  * `SFTP_PASSWORD`, associated password for user
  * `SFTP_SSH_PRIVATE_KEY`, cryptographic private key authorized from the host to `SFTP_USER`. It must be **base64 encoded**. For example: `SFTP_SSH_PRIVATE_KEY=$(base64 -w0 < /home/localuser/.ssh/myprivatekeytoaccessremotesftp)`. Optionally `SFTP_PASSPHRASE`can be provided together with the private key login.

### AWS S3 bucket

Optionally, AWS env variables can be provided from an assummed session, or do not provied at all if inside EC2 and allow
the assumed role to be used.

### MINIO bucket

Probably works against Minio buckets providing `AWS_S3_ENDPOINT_URL` variable, but not tested.
