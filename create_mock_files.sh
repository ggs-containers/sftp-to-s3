#!/bin/bash
mkdir -p mock_dir/subdir;
for i in {1..30}; do 
fallocate -l 1M mock_dir/mockfile_${i}; 
done
for i in {1..5}; do 
fallocate -l 50M mock_dir/subdir/subdir_mockfile_${i}; 
done

fallocate -l 1M mock_dir/fail_to_download
chmod u-r mock_dir/fail_to_download
# fallocate -l 1M mock_dir/fail_to_delete
# chmod 755 mock_dir/fail_to_delete
