# BUILD STAGE
FROM rust:1.68-bookworm AS builder

RUN rustup target add x86_64-unknown-linux-musl x86_64-unknown-linux-gnu
RUN apt-get update && apt-get install -y musl-tools openssl libssl-dev pkg-config

WORKDIR /usr/src/build

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
COPY ./src ./src

# RUN cargo build --release
RUN cargo install --locked --target x86_64-unknown-linux-gnu --bin sftp_to_s3 --path .


# BUNDLE STAGE
FROM debian:bookworm

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates nload pkg-config libssl-dev openssl
RUN update-ca-certificates

RUN useradd -ms /bin/bash exec_user
USER exec_user
WORKDIR /home/exec_user
COPY --from=builder /usr/local/cargo/bin/sftp_to_s3 .

ENV RUST_LOG info
CMD ["./sftp_to_s3"]