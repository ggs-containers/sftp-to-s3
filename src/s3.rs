use aws_config::meta::region::RegionProviderChain;
use aws_sdk_s3::operation::put_object::PutObjectError;
use aws_sdk_s3::primitives::ByteStream;
use aws_sdk_s3::types::{ObjectCannedAcl, StorageClass};
use aws_sdk_s3::Client;
use aws_sdk_sts::Client as StsClient;
use aws_types::region::Region;

use futures::StreamExt;
use log::{debug, error, info, warn};
use std::path::Path;
use std::str;

pub async fn get_aws_s3_client(bucket_region: Option<String>) -> Client {
    let region_provider = RegionProviderChain::first_try(bucket_region.map(Region::new))
        .or_default_provider()
        .or_else(Region::new("eu-west-1"));

    let config = aws_config::from_env().region(region_provider).load().await;
    // let config = aws_config::from_env().load().await;
    let s3_client = Client::new(&config);
    let sts_client = StsClient::new(&config);
    let caller_identity = sts_client.get_caller_identity().send().await;
    match caller_identity {
        Ok(_) => {
            info!("AWS IDENTITY: {:?}", &caller_identity);
        }
        Err(msg) => {
            warn!(
                "AWS IDENTITY WITH STS MODULE COULD NOT BE CALCULATED WITH MESSAGE {:?}",
                &msg
            );
        }
    }
    s3_client
}

pub async fn upload_object_to_s3(
    client: &Client,
    bucket_name: &str,
    file_path: &Path,
    key: &str,
    acl: Option<ObjectCannedAcl>,
    storage_class: Option<StorageClass>,
) -> Result<(), aws_sdk_s3::error::SdkError<PutObjectError>> {
    let body = ByteStream::from_path(file_path).await;
    let upload_result = client
        .put_object()
        .bucket(bucket_name)
        .key(key)
        .set_acl(acl)
        .set_storage_class(storage_class)
        .body(body.unwrap())
        .send()
        .await;

    match upload_result {
        Ok(_) => {
            debug!(
                "Uploaded file: {:?} to bucket {} and key {}",
                file_path, bucket_name, key
            );
            Ok(())
        }
        Err(err) => {
            error!(
                "Error {:?} when uploading file: {:?} to bucket {} and key {}",
                err, file_path, bucket_name, key
            );
            Err(err)
        }
    }
}

/// It returns lastmodified unix timestamp if file exists, otherwise 0
pub async fn get_object_last_modified(client: &Client, bucket_name: &str, key: &str) -> usize {
    let res = client
        .get_object_attributes()
        .bucket(bucket_name)
        .key(key)
        .send()
        .await;
    match res {
        Ok(obj) => obj
            .last_modified()
            .map_or(0, |lm| lm.as_secs_f64() as usize),
        _ => 0,
    }
}

// https://github.com/awslabs/aws-sdk-rust/issues/710
pub async fn list_s3_objects(
    client: &Client,
    bucket_name: &str,
    prefix: &str,
) -> Vec<aws_sdk_s3::types::Object> {
    let mut list_generator = client
        .list_objects_v2()
        .bucket(bucket_name)
        .prefix(prefix)
        .into_paginator()
        .send();
    let mut results: Vec<aws_sdk_s3::types::Object> = Vec::new();
    while let Some(obj_list) = list_generator.next().await {
        if let Some(s3_objs) = obj_list.unwrap().contents() {
            let mut aux_vec: Vec<aws_sdk_s3::types::Object> = s3_objs.to_vec();
            results.append(&mut aux_vec);
        }
    }
    results
}
