use super::s3::list_s3_objects;
use super::sftp::{build_s3_key, DiffKind};
use aws_sdk_s3::Client;
use log::info;
use ssh2;
use std::collections::{HashMap, HashSet};
use std::path::{Path, PathBuf};

pub async fn filter_diff(
    sftp_paths: &[(PathBuf, ssh2::FileStat)],
    bucket_name: &str,
    bucket_prefix: &str,
    s3_client: &Client,
    diff_kind: &DiffKind,
) -> Vec<(PathBuf, ssh2::FileStat)> {
    let dirnames: HashSet<String> = sftp_paths
        .iter()
        .map(|(p, _)| {
            let s3_str_key = build_s3_key(p, bucket_prefix);
            let s3_key = Path::new(&s3_str_key);
            s3_key
                .parent()
                .unwrap_or(Path::new(""))
                .to_str()
                .unwrap()
                .to_string()
        })
        .collect();
    info!("Obtained directories to list for diff {dirnames:?}, listing objects in them");
    let mut s3_objs: HashMap<String, aws_sdk_s3::types::Object> = HashMap::new();
    for dir in dirnames.iter() {
        let dir_objs = list_s3_objects(s3_client, bucket_name, dir).await;
        s3_objs.reserve(dir_objs.len());
        for obj in dir_objs {
            if let Some(key) = obj.key() {
                s3_objs.insert(key.to_string(), obj);
            }
        }
    }
    info!("Listed a total of {} objects in s3://{bucket_name}/{bucket_prefix} related to source sftp files", s3_objs.len());
    let valid_paths: Vec<(PathBuf, ssh2::FileStat)> = sftp_paths
        .iter()
        .filter(|(sftp_path, filestat)| {
            let s3_str_key = build_s3_key(sftp_path, bucket_prefix);
            match diff_kind {
                DiffKind::NoDiff => true,
                DiffKind::DiffExisting => !s3_objs.contains_key(&s3_str_key),
                DiffKind::DiffGreatEqualLastModified => {
                    let sftp_mtime = filestat.mtime.unwrap_or(9999999999999999999) as usize;
                    let s3_mtime: usize = if let Some(s3_obj) = s3_objs.get(&s3_str_key) {
                        match s3_obj.last_modified() {
                            Some(mtime) => mtime.as_secs_f64() as usize,
                            None => 0,
                        }
                    } else {
                        0
                    };
                    s3_mtime <= sftp_mtime
                }
            }
        })
        .map(|(sftp_path, filestat)| (sftp_path.clone(), filestat.clone()))
        .collect();
    info!(
        "Obtained {} valid paths with {:?} criteria to copy",
        valid_paths.len(),
        diff_kind
    );
    valid_paths
}
