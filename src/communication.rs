use super::errors;
use std::default::Default;
use std::path::PathBuf;

#[derive(Debug)]
pub struct Message {
    pub sftp_path: PathBuf,
    pub file_size_in_mb: u64,
    pub local_path: Option<PathBuf>,
    pub n_task: Option<u64>,
    pub process_error: Option<errors::ProcessError>,
    pub success: bool,
}

impl Default for Message {
    fn default() -> Self {
        Message {
            sftp_path: PathBuf::new(),
            file_size_in_mb: 0,
            local_path: None,
            n_task: None,
            process_error: None,
            success: true,
        }
    }
}
