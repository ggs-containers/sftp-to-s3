use regex::{Regex, RegexBuilder};

pub fn build_regex(pattern: &str) -> Regex {
    let aux_result = RegexBuilder::new(pattern).build();
    if let Err(err) = aux_result {
        let error_msg = err.to_string();
        let aux_panic_msg = format!(
            "Failed to build path filtering regex pattern {} with error {}",
            pattern,
            error_msg.as_str()
        );
        let panic_msg = aux_panic_msg.as_str();
        panic!("{}", panic_msg)
    }
    aux_result.unwrap()
}

pub fn filter_by_regex(text: &str, regex: &Regex) -> bool {
    regex.is_match(text)
}
