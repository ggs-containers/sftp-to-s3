use sftp_to_s3::logging::get_log_builder;


#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let mut builder = get_log_builder();
    builder.init();

    sftp_to_s3::run().await;
}
