use crossbeam_channel::{unbounded, Receiver, Sender};
use std::thread::{sleep, spawn};
use std::time::Duration;

fn main() {
    let (tx1, rx1) = unbounded();
    let (tx2, rx2) = unbounded();
    for i in 0..3 {
        let tx1 = tx1.clone();
        let rx2 = rx2.clone();
        spawn(move || consumer(i, tx1, rx2));
    }
    let vec_int: Vec<u64> = vec![3, 1, 2, 2, 1, 3, 4, 4, 2];
    spawn(move || producer(vec_int, rx1, tx2));
    // loop {}
}

fn consumer(thread: i32, request: Sender<bool>, response: Receiver<u64>) {
    let mut receive_counter = 3;
    loop {
        request.send(true).unwrap();
        let r = response.recv().unwrap();
        println!("Task {} received {}", thread, r);
        receive_counter -= 1;
        if receive_counter == 0 {
            println!("Task {} is done!", thread);
            break;
        } else {
            sleep(Duration::from_secs(r))
        }
    }
}

fn producer(mut vec_u64: Vec<u64>, request: Receiver<bool>, response: Sender<u64>) {
    loop {
        if request.try_recv().is_ok() {
            let send_val = vec_u64.swap_remove(0);
            response.send(send_val).unwrap();
            if vec_u64.is_empty() {
                println!("Finishing producing");
                break;
            }
        }
    }
}
