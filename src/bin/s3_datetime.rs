use aws_sdk_s3::primitives::DateTime;
use aws_smithy_types::date_time::Format;

fn main() {
    let original_unix_epoch = "1681731508";
    let dt = DateTime::from_str(original_unix_epoch, Format::EpochSeconds).unwrap();
    let fmt_dt = dt.fmt(Format::DateTime).unwrap();
    let back_to_unix_epoch = dt.as_secs_f64() as usize;

    println!("Original unix epoch {original_unix_epoch}");
    println!("{dt:?}");
    println!("{fmt_dt:?}");
    println!("{back_to_unix_epoch:?}");
}
