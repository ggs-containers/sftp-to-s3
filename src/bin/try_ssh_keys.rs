use ssh2::Session;

fn main() {
    // Almost all APIs require a `Session` to be available
    let sess = Session::new().unwrap();
    let mut agent = sess.agent().unwrap();

    // Connect the agent and request a list of identities
    agent.connect().unwrap();
    let identities_list = agent.list_identities().unwrap();
    println!("{:?}", identities_list);
    println!("{:?}", agent.identities());

    // for identity in agent.identities() {
    //     let identity = identity.unwrap(); // assume no I/O errors
    //     println!("{}", identity.comment());
    //     let pubkey = identity.blob();
    // }
}
