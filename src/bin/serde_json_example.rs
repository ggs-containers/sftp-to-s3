use serde_json::json;
use std::fs;
use std::path::Path;

fn main() {
    // The type of `john` is `serde_json::Value`
    let phones = vec!["+44 1234567", "+44 2345678"];
    let john = json!({
        "name": "John Doe",
        "age": 43,
        "phones": phones
    });

    println!("first phone number: {}", john["phones"][0]);

    // Convert to a string of JSON and print it out
    println!("{}", john);

    let output_result_path = Path::new("/tmp/sftp_to_s3_results.json");
    fs::write(output_result_path, john.to_string()).unwrap();
}
