use super::errors::SftpDownloadError;
use super::filter::{build_regex, filter_by_regex};
use async_io::Async;
use async_ssh2_lite as assh2;
use base64::engine::general_purpose as b64_decode;
use base64::Engine;
use log::{debug, info};
use ssh2;
use std::collections::HashSet;
use std::env;
use std::fs as std_fs;
use std::io as std_io;
use std::io::{BufReader, BufWriter};
use std::net::{TcpStream, ToSocketAddrs};
use std::path::{Path, PathBuf};
use std::str;
use std::str::FromStr;
use std::time::Duration;
use std::time::Instant;
use tokio::fs;
use tokio::io;
use tokio_util::compat::FuturesAsyncReadCompatExt;

// https://github.com/alexcrichton/ssh2-rs/issues/166#issuecomment-586703237
// const SFTP_BUFFER_BYTES_SIZE: usize = 1024 * 1024 * 16;
const VERY_LONG_TIME_IN_MS: u32 = 3600 * 1000;

#[derive(Debug, Clone)]
pub enum DiffKind {
    NoDiff,
    DiffExisting,
    DiffGreatEqualLastModified,
}

impl FromStr for DiffKind {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lowercased_s = s.to_lowercase();
        let val = match lowercased_s.as_str() {
            "diffexisting" => DiffKind::DiffExisting,
            "diffgreatequallastmodified" => DiffKind::DiffGreatEqualLastModified,
            _ => DiffKind::NoDiff,
        };
        Ok(val)
    }
}

pub enum SftpSession {
    SyncSftpSession(ssh2::Session, ssh2::Sftp),
    AsyncSftpSession(assh2::AsyncSession<TcpStream>, assh2::AsyncSftp<TcpStream>),
}

impl SftpSession {
    pub fn get_sftp_session(&self) -> Result<&ssh2::Sftp, ()> {
        match &self {
            Self::SyncSftpSession(_, sftp_sess) => Ok(sftp_sess),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone)]
pub enum SftpGetOption {
    Get,
    Delete,
}

impl FromStr for SftpGetOption {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lowercased_s = s.to_lowercase();
        let val = match lowercased_s.as_str() {
            "get" => SftpGetOption::Get,
            "delete" => SftpGetOption::Delete,
            _ => SftpGetOption::Get,
        };
        Ok(val)
    }
}

#[derive(Debug, Clone)]
pub enum SftpDownloadMode {
    Sftp,
    Scp,
    AsyncSftp,
}

impl FromStr for SftpDownloadMode {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lowercased_s = s.to_lowercase();
        let val = match lowercased_s.as_str() {
            "sftp" => SftpDownloadMode::Sftp,
            "scp" => SftpDownloadMode::Scp,
            "asyncsftp" => SftpDownloadMode::AsyncSftp,
            _ => SftpDownloadMode::Sftp,
        };
        Ok(val)
    }
}

pub struct SftpConfig {
    pub sftp_host_port: String,
    pub sftp_user: String,
    sftp_password: Option<String>,
    sftp_private_key: Option<String>,
    sftp_passphrase: Option<String>,
}

impl SftpConfig {
    pub fn new_from_env() -> Self {
        let sftp_host =
            env::var("SFTP_HOST").expect("Env var SFTP_HOST is mandatory but not present");
        let sftp_port =
            env::var("SFTP_PORT").expect("Env var SFTP_PORT is mandatory but not present");
        let sftp_host_port = format!("{}:{}", sftp_host, sftp_port);

        let sftp_user =
            env::var("SFTP_USER").expect("Env var SFTP_USER is mandatory but not present");

        let sftp_password = env::var("SFTP_PASSWORD").ok();
        let sftp_passphrase = env::var("SFTP_PASSPHRASE").ok();
        let sftp_rsa_key_raw = env::var("SFTP_SSH_PRIVATE_KEY").ok();
        let sftp_private_key = match sftp_rsa_key_raw {
            Some(val) => {
                let bytes = b64_decode::STANDARD_NO_PAD.decode(val).unwrap();
                Some(str::from_utf8(&bytes).unwrap().replace(r"\n", ""))
            }
            _ => None,
        };

        Self {
            sftp_host_port,
            sftp_user,
            sftp_password,
            sftp_private_key,
            sftp_passphrase,
        }
    }

    pub fn get_sftp_connection(&self) -> SftpSession {
        info!("Connecting to SFTP server");
        let connection_timeout = Duration::new(30, 0);
        let socket_addr = &self
            .sftp_host_port
            .to_socket_addrs()
            .unwrap()
            .next()
            .unwrap();
        let start = Instant::now();
        let tcp = TcpStream::connect_timeout(socket_addr, connection_timeout)
            .expect("Problem with TCP conectivity against host and port provided");
        info!("Tcp stream read timeout is: {:?}", tcp.read_timeout());
        info!("Tcp stream write timeout is: {:?}", tcp.write_timeout());

        let mut sess = ssh2::Session::new().unwrap();
        sess.set_timeout(VERY_LONG_TIME_IN_MS);
        sess.set_compress(true);
        sess.set_tcp_stream(tcp);
        info!(
            "SSH2 session timeout in miliseconds is: {:?}",
            sess.timeout()
        );

        sess.handshake()
            .expect("SSH session handshake before login failed");
        match &self.sftp_private_key {
            Some(val) => {
                let passphrase: Option<&str> = match &self.sftp_passphrase {
                    Some(pass) => Some(pass),
                    _ => None,
                };
                info!("Using private cryptographic key to SSH login");
                sess.userauth_pubkey_memory(&self.sftp_user, None, val, passphrase)
                    .expect("SSH authentication failed");
            }
            _ => {
                info!("Using password to SSH login");
                sess.userauth_password(&self.sftp_user, self.sftp_password.as_ref().unwrap())
                    .expect("SSH authentication failed");
            }
        };
        assert!(sess.authenticated());
        let sftp_session = sess
            .sftp()
            .expect("Failed to retrieve SFTP session from SSH session");
        let match_duration = start.elapsed();
        info!(
            "Final time elapsed connecting to SFTP server is: {:?}",
            match_duration
        );
        SftpSession::SyncSftpSession(sess, sftp_session)
    }

    pub async fn get_async_sftp_connection(&self) -> SftpSession {
        info!("Connecting to SFTP server");
        let socket_addr = self
            .sftp_host_port
            .to_socket_addrs()
            .unwrap()
            .next()
            .unwrap();
        let start = Instant::now();
        let tcp = Async::<TcpStream>::connect(socket_addr)
            .await
            .expect("Problem with TCP conectivity against host and port provided");
        let mut sess = assh2::AsyncSession::new(tcp, None).unwrap();
        sess.handshake()
            .await
            .expect("SSH session handshake before login failed");
        match &self.sftp_private_key {
            Some(val) => {
                let passphrase: Option<&str> = match &self.sftp_passphrase {
                    Some(pass) => Some(pass),
                    _ => None,
                };
                info!("Using private cryptographic key to SSH login");
                sess.userauth_pubkey_memory(&self.sftp_user, None, val, passphrase)
                    .await
                    .expect("SSH authentication failed");
            }
            _ => {
                info!("Using password to SSH login");
                sess.userauth_password(&self.sftp_user, self.sftp_password.as_ref().unwrap())
                    .await
                    .expect("SSH authentication failed");
            }
        };
        assert!(sess.authenticated());
        let sftp_session = sess
            .sftp()
            .await
            .expect("Failed to retrieve SFTP session from SSH session");
        let match_duration = start.elapsed();
        info!(
            "Final time elapsed connecting to SFTP server is: {:?}",
            match_duration
        );
        SftpSession::AsyncSftpSession(sess, sftp_session)
    }
}

pub fn delete_sftp_path(path: &Path, sftp_sess: &ssh2::Sftp) -> Result<(), ssh2::Error> {
    sftp_sess.unlink(path)
}

pub async fn delete_local_path(path: &Path) {
    let _ = fs::remove_file(path).await;
}

pub async fn delete_local_dir(path: &Path) {
    let _ = fs::remove_dir_all(path).await;
}

pub fn build_writing_path(path: &Path, local_dir: &Path) -> PathBuf {
    let transformed_sftp_path = match path.strip_prefix("/") {
        Ok(val) => val,
        Err(_) => path,
    };
    local_dir.join(transformed_sftp_path)
}

pub fn list_sftp_dir_and_filter(
    sftp_dir: &Path,
    sftp_sess: &ssh2::Sftp,
    min_mtime: u64,
    required_pattern: &Option<String>,
) -> Vec<(std::path::PathBuf, ssh2::FileStat)> {
    let start = Instant::now();
    info!("Listing path {} recursively", sftp_dir.to_str().unwrap());

    let pattern_to_apply = required_pattern
        .as_ref()
        .map(|pattern| build_regex(pattern));

    let mut n_total_mbytes: u64 = 0;
    let mut final_paths: Vec<(PathBuf, ssh2::FileStat)> = Vec::new();
    let mut aux_dirs: HashSet<PathBuf> = HashSet::new();
    aux_dirs.insert(sftp_dir.to_path_buf());
    'main: loop {
        if aux_dirs.is_empty() {
            break 'main;
        }
        let mut temp_aux_dirs: HashSet<PathBuf> = HashSet::new();
        for dir in aux_dirs.drain() {
            info!("\t Listing directory {:?}", dir);
            let aux_path_list = sftp_sess.readdir(dir.as_path()).unwrap();
            for element in aux_path_list {
                let filestat = &element.1;
                let is_directory = filestat.is_dir();
                let mtime = filestat.mtime.unwrap_or(9999999999999999999);
                let fills_pattern = match pattern_to_apply {
                    None => true,
                    Some(ref built_regex) => {
                        let aux_path = &element.0.to_string_lossy().to_string();
                        filter_by_regex(aux_path, built_regex)
                    }
                };
                if mtime >= min_mtime && !is_directory && fills_pattern {
                    final_paths.push(element.clone());
                    n_total_mbytes += filestat.size.unwrap_or(0);
                }
                if is_directory {
                    temp_aux_dirs.insert(element.0);
                }
            }
        }
        aux_dirs.extend(temp_aux_dirs)
        // for path in temp_aux_dirs {
        //     aux_dirs.insert(path);
        // }
    }
    n_total_mbytes /= 1048576;
    let match_duration = start.elapsed();
    info!(
        "Listed a total valid list with {} files with a total size of {} megabytes in {:?}",
        final_paths.len(),
        n_total_mbytes,
        match_duration
    );
    final_paths
}

pub fn build_s3_key(sftp_path: &Path, bucket_prefix: &str) -> String {
    let corrected_sftp_path = sftp_path.strip_prefix("/").unwrap_or(sftp_path);
    let mut key_buf = PathBuf::new();
    key_buf.push(bucket_prefix);
    key_buf.push(corrected_sftp_path);
    let s3_key: String = String::from(key_buf.to_str().unwrap());
    s3_key
}

/// Use large buffers for the copy operation, leaving it to std::io::copy to handle the slightly
/// fiddly short read/write cases.
/// As-written, this will consume the input handles, but you could decompose this and just
/// wrap them up elsewhere in the program
fn copy_with_large_buffer<R, W>(source: R, destination: W, buf_size: usize) -> std::io::Result<u64>
where
    R: std_io::Read,
    W: std_io::Write,
{
    let mut buf_source = BufReader::with_capacity(buf_size, source);
    let mut buf_destination = BufWriter::with_capacity(buf_size, destination);
    std_io::copy(&mut buf_source, &mut buf_destination)
}

pub async fn download_file_from_scp(
    path: &Path,
    ssh_sess: &ssh2::Session,
    local_folder: &Path,
    buffer_size: usize,
) -> Result<PathBuf, SftpDownloadError> {
    let (mut remote_file, _): (ssh2::Channel, ssh2::ScpFileStat) = match ssh_sess.scp_recv(path) {
        Ok((channel, filestat)) => (channel, filestat),
        Err(msg) => {
            return Err(SftpDownloadError::CantOpen(
                path.to_path_buf(),
                msg.to_string(),
            ))
        }
    };

    let writing_path = build_writing_path(path, local_folder);
    let parent_path = writing_path.parent().unwrap();
    if let Err(msg) = fs::create_dir_all(parent_path).await {
        return Err(SftpDownloadError::CantWriteLocally(
            parent_path.to_path_buf(),
            msg.to_string(),
        ));
    }

    let copying_process = copy_with_large_buffer(
        &mut remote_file,
        std_fs::File::create(&writing_path).unwrap(),
        buffer_size,
    );

    let n_bytes: f64 = match copying_process {
        Ok(n_bytes) => n_bytes as f64,
        Err(msg) => {
            return Err(SftpDownloadError::CantRead(
                path.to_path_buf(),
                msg.to_string(),
            ))
        }
    };

    if let Err(msg) = remote_file.send_eof() {
        return Err(SftpDownloadError::CantRead(
            path.to_path_buf(),
            msg.to_string(),
        ));
    }

    if let Err(msg) = remote_file.wait_eof() {
        return Err(SftpDownloadError::CantRead(
            path.to_path_buf(),
            msg.to_string(),
        ));
    }

    if let Err(msg) = remote_file.close() {
        return Err(SftpDownloadError::CantRead(
            path.to_path_buf(),
            msg.to_string(),
        ));
    }

    if let Err(msg) = remote_file.wait_close() {
        return Err(SftpDownloadError::CantRead(
            path.to_path_buf(),
            msg.to_string(),
        ));
    }

    let n_megabytes: f64 = (n_bytes / 1000000.0 * 100.0).round() / 100.0;

    debug!("Downloading {:?} with {} MB", writing_path, n_megabytes);

    Ok(writing_path.to_path_buf())
}

pub async fn download_file_from_sftp(
    path: &Path,
    sftp_sess: &ssh2::Sftp,
    local_folder: &Path,
    buffer_size: usize,
) -> Result<PathBuf, SftpDownloadError> {
    let mut open_file =
        match sftp_sess.open_mode(path, ssh2::OpenFlags::READ, 0o644, ssh2::OpenType::File) {
            Ok(file) => file,
            Err(msg) => {
                return Err(SftpDownloadError::CantOpen(
                    path.to_path_buf(),
                    msg.to_string(),
                ))
            }
        };

    let writing_path = build_writing_path(path, local_folder);
    let parent_path = writing_path.parent().unwrap();
    if let Err(msg) = fs::create_dir_all(parent_path).await {
        return Err(SftpDownloadError::CantWriteLocally(
            parent_path.to_path_buf(),
            msg.to_string(),
        ));
    }

    // https://github.com/alexcrichton/ssh2-rs/blob/master/src/sftp.rs#L716
    // this Read trait will take into account input buffer length to operate on reading
    let copying_process = copy_with_large_buffer(
        &mut open_file,
        std_fs::File::create(&writing_path).unwrap(),
        buffer_size,
    );

    let n_bytes: f64 = match copying_process {
        Ok(n_bytes) => n_bytes as f64,
        Err(msg) => {
            return Err(SftpDownloadError::CantRead(
                path.to_path_buf(),
                msg.to_string(),
            ))
        }
    };

    let n_megabytes: f64 = (n_bytes / 1000000.0 * 100.0).round() / 100.0;
    debug!("Downloading {:?} with {} MB", writing_path, n_megabytes);
    Ok(writing_path.to_path_buf())
}

pub async fn async_download_file_from_sftp(
    path: &Path,
    sftp_sess: &assh2::AsyncSftp<TcpStream>,
    local_folder: &Path,
) -> Result<PathBuf, SftpDownloadError> {
    let writing_path = build_writing_path(path, local_folder);
    let parent_path = &writing_path.parent().unwrap();
    if let Err(msg) = fs::create_dir_all(parent_path).await {
        return Err(SftpDownloadError::CantWriteLocally(
            parent_path.to_path_buf(),
            msg.to_string(),
        ));
    }
    let mut local_file = fs::File::create(&writing_path).await.unwrap();

    let mut open_file = match sftp_sess.open(path).await {
        Ok(file) => file.compat(),
        Err(msg) => {
            return Err(SftpDownloadError::CantOpen(
                path.to_path_buf(),
                msg.to_string(),
            ))
        }
    };

    let n_bytes: f64 = io::copy(&mut open_file, &mut local_file).await.unwrap() as f64;
    let n_megabytes: f64 = (n_bytes / 1000000.0 * 100.0).round() / 100.0;
    debug!("Downloading {:?} with {} MB", writing_path, n_megabytes);
    Ok(writing_path.to_path_buf())
}
