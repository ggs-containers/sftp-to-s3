use chrono::Local;
use env_logger;
use log::info;
use std::io::Write;
use std::time;

pub struct LogProgressBar {
    start_time: time::Instant,
    total_elements: usize,
    counter: usize,
}

impl LogProgressBar {
    pub fn new(total_elements: usize) -> Self {
        let start_time = time::Instant::now();
        Self {
            start_time,
            total_elements,
            counter: 0,
        }
    }

    pub fn inc(&mut self) {
        self.counter += 1;
        let now_instant = time::Instant::now();
        let elapsed_time_duration = now_instant - self.start_time;
        let elapsed_time_duration_secs = elapsed_time_duration.as_secs();

        let percentage = (self.counter as f64 / self.total_elements as f64) * 100.0;
        let total_bars = 20.0;
        let done_bars = ((self.counter as f64 / self.total_elements as f64) * total_bars) as usize;
        let remaining_bars = total_bars as usize - done_bars;
        let mut bar_str = String::from("[");
        for _ in 0..done_bars {
            bar_str.push('|');
        }
        for _ in 0..remaining_bars {
            bar_str.push(' ');
        }
        bar_str.push(']');
        let et_seconds = elapsed_time_duration_secs % 60;
        let et_minutes = (elapsed_time_duration_secs / 60) % 60;
        let et_hours = (elapsed_time_duration_secs / 60) / 60;

        let remaining_time_seconds =
            (elapsed_time_duration_secs as f64 * (100.0 - percentage) / percentage) as usize;
        let rt_seconds = remaining_time_seconds % 60;
        let rt_minutes = (remaining_time_seconds / 60) % 60;
        let rt_hours = (remaining_time_seconds / 60) / 60;

        info!(
            "PROGRESS BAR: {} {number:.2} %, Elapsed: {et_hours}h:{et_minutes}m:{et_seconds}s ETA: {rt_hours}h:{rt_minutes}m:{rt_seconds}s",
            bar_str,
            number = percentage,
            et_hours = et_hours,
            et_minutes = et_minutes,
            et_seconds = et_seconds,
            rt_hours = rt_hours,
            rt_minutes = rt_minutes,
            rt_seconds = rt_seconds
        );
    }
}

pub fn get_log_builder() -> env_logger::Builder {
    let mut builder = env_logger::Builder::from_env("RUST_LOG");

    builder.format(|buf, record| {
        writeln!(
            buf,
            "{} {}: {}",
            record.level(),
            Local::now().format("%Y-%m-%dT%H:%M:%S.%3f"),
            record.args()
        )
    });
    builder
}
