use aws_sdk_s3::types::{ObjectCannedAcl, StorageClass};
use clap::Parser;

use aws_sdk_s3::Client as S3Client;
use futures::future::join_all;
use std::fmt;
use std::fs;

use std::path::{Path, PathBuf};
use std::time;

use errors::ProcessError;
use log::{debug, error, info};
use logging::LogProgressBar;
use s3::{get_aws_s3_client, upload_object_to_s3};
use serde_json::json;
use sftp::{
    async_download_file_from_sftp, delete_local_path, delete_sftp_path, download_file_from_scp,
    list_sftp_dir_and_filter, DiffKind, SftpConfig, SftpGetOption, SftpSession,
};
use std::collections::{BTreeMap, HashMap};
use std::sync::mpsc;
use tokio::task::JoinHandle;
use tokio::time::{sleep, Duration};

use communication::Message;

use crate::sftp::download_file_from_sftp;
pub mod communication;
pub mod diff_load;
pub mod errors;
pub mod filter;
pub mod logging;
pub mod s3;
pub mod sftp;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// path of the sftp remote filesystem to ingest
    #[clap(long, value_parser)]
    pub sftp_source_path: String,

    /// date to ingest from in linux timestamp (date +%s) to be compared
    /// with file MTIME
    #[clap(long, value_parser, default_value_t = 0)]
    pub sftp_last_mtime: u64,

    /// regex to filter valid processing paths from origin sftp. The path must verify the regex to be loaded and transfered.
    /// For example: `"_\\d{2}$|fail_to_download|subdir"`
    #[clap(long, value_parser, default_value = "None")]
    pub sftp_filter_regex: String,

    /// number of parallel sftp sessions to perform download
    #[clap(long, value_parser, default_value_t = 3)]
    pub sftp_n_sessions: u64,

    /// size of sftp copying buffer in bytes
    #[clap(long, value_parser, default_value_t = 16777216)]
    pub sftp_buffer_size: usize,

    /// summary json output path
    #[clap(long, value_parser, default_value = "/tmp/sftp_to_s3_results.json")]
    pub output_json_path: String,

    /// local temporal auxiliar folder where download data
    #[clap(long, value_parser, default_value = "/tmp/__sftp_download__")]
    pub local_folder: String,

    /// wether if delete files from sftp after correct upload to s3,
    /// possible values are get and delete
    #[clap(long, value_parser, default_value = "GET")]
    pub sftp_get_option: String,

    // wether if compare existence or last modified time between sftp and s3 files,
    /// possible values are:
    /// - diffgreatequallastmodified (DEFAULT): it only copies files if last modified time on sftp is great or equal than s3 last modified time
    /// - diffexisting: it only copies files if file does not exist in s3
    /// - nodiff: it always copies independently of s3 existence or last modified time on target key
    #[clap(long, value_parser, default_value = "diffgreatequallastmodified")]
    pub sftp_diff_option: String,

    /// one of those values (if not provided it takes default value):
    /// - `sftp`: to perform download using sftp client (slower).
    /// - `scp`: to perform download using scp (faster but not available always). **DEFAULT**
    /// - `asyncsftp`: to perform download using async sftp session (testing, not really performant)
    #[clap(long, value_parser, default_value = "scp")]
    pub sftp_download_mode: String,

    /// target bucket name
    #[clap(long, value_parser)]
    pub bucket_name: String,

    /// target bucket prefix
    #[clap(long, value_parser)]
    pub bucket_prefix: String,

    /// target bucket region
    #[clap(long, value_parser, default_value = "eu-west-1")]
    pub bucket_region: String,

    /// target bucket storage class of files copied, a value from
    /// https://docs.rs/aws-sdk-s3/latest/aws_sdk_s3/model/enum.StorageClass.html and
    /// https://docs.rs/aws-sdk-s3/latest/src/aws_sdk_s3/model.rs.html#36-49
    #[clap(long, value_parser, default_value = "STANDARD")]
    pub bucket_files_storage_class: String,

    /// target bucket ACL of files copied, a value from
    /// https://docs.rs/aws-sdk-s3/latest/aws_sdk_s3/model/enum.ObjectCannedAcl.html and
    /// https://docs.rs/aws-sdk-s3/latest/src/aws_sdk_s3/model.rs.html#3245-3257
    #[clap(long, value_parser, default_value = "bucket-owner-full-control")]
    pub bucket_files_acl: String,
}
impl Args {
    fn get_filter_regex(&self) -> Option<String> {
        match self.sftp_filter_regex.as_ref() {
            "None" => None,
            _ => Some(self.sftp_filter_regex.clone()),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ProcessInfo {
    local_folder_path: PathBuf,
    pub sftp_last_mtime: u64,
    pub sftp_filter_regex: Option<String>,
    pub sftp_get_option: SftpGetOption,
    pub sftp_diff_option: DiffKind,
    pub sftp_download_mode: sftp::SftpDownloadMode,
    sftp_source_path: PathBuf,
    pub bucket_name: String,
    pub bucket_prefix: String,
    pub bucket_region: String,
    pub bucket_files_storage_class: StorageClass,
    pub bucket_files_acl: ObjectCannedAcl,
    pub sftp_n_sessions: u64,
    pub sftp_buffer_size: usize,
    pub output_json_path: PathBuf,
}

impl ProcessInfo {
    pub fn new() -> ProcessInfo {
        let args = Args::parse();
        let local_folder_path = PathBuf::from(&args.local_folder);
        let sftp_source_path = PathBuf::from(&args.sftp_source_path);
        let output_json_path = PathBuf::from(&args.output_json_path);
        let sftp_get_option: SftpGetOption = args.sftp_get_option.parse().unwrap();
        let sftp_diff_option: DiffKind = args.sftp_diff_option.parse().unwrap();
        let bucket_files_storage_class =
            StorageClass::from(args.bucket_files_storage_class.as_str());
        let bucket_files_acl = ObjectCannedAcl::from(args.bucket_files_acl.as_str());
        let sftp_last_mtime = args.sftp_last_mtime;
        let sftp_filter_regex: Option<String> = args.get_filter_regex();
        let sftp_n_sessions = args.sftp_n_sessions;
        let sftp_buffer_size = args.sftp_buffer_size;
        let bucket_name = args.bucket_name;
        let bucket_prefix = args.bucket_prefix;
        let bucket_region = args.bucket_region;

        let sftp_download_mode = args
            .sftp_download_mode
            .parse()
            .unwrap_or(sftp::SftpDownloadMode::Scp);

        Self {
            local_folder_path,
            sftp_last_mtime,
            sftp_filter_regex,
            sftp_get_option,
            sftp_diff_option,
            sftp_download_mode,
            sftp_source_path,
            bucket_name,
            bucket_prefix,
            bucket_region,
            bucket_files_storage_class,
            bucket_files_acl,
            sftp_n_sessions,
            sftp_buffer_size,
            output_json_path,
        }
    }

    pub fn get_local_folder_path(&self) -> &Path {
        self.local_folder_path.as_path()
    }

    pub async fn set_auxiliar_local_folder(&self) {
        let local_folder_path = &self.get_local_folder_path();
        info!(
            "Creating local folder to handle the process: {}",
            local_folder_path.to_str().unwrap()
        );
        sftp::delete_local_dir(local_folder_path).await;
        fs::create_dir_all(local_folder_path).unwrap();
    }

    pub async fn delete_auxiliar_local_folder(&self) {
        let local_folder_path = &self.get_local_folder_path();
        info!(
            "Deleting local folder that handled the process: {}",
            local_folder_path.to_str().unwrap()
        );
        sftp::delete_local_dir(local_folder_path).await;
    }

    pub fn get_sftp_source_path(&self) -> &Path {
        self.sftp_source_path.as_path()
    }

    pub fn get_output_json_path(&self) -> &Path {
        self.output_json_path.as_path()
    }
}

impl fmt::Display for ProcessInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "ProcessInfo:\n\tsftp_source_path: {},\n\tbucket_region: {},\n\tbucket_name: {},\
        \n\tbucket_prefix: {},\n\tbucket_files_storage_class: {:?},\n\tbucket_files_acl: {:?},\n\
        \tsftp_last_mtime: {},\n\tsftp_filter_regex: {:?},\n\tsftp_get_option: {:?},\n\tsftp_diff_option: {:?},\n\tsftp_download_mode: {:?},\
        \n\tlocal_folder_path: {:?},\n\toutput_json_path: {:?},\n\tsftp_n_sessions: {},\n\tsftp_buffer_size: {}",
            self.sftp_source_path.to_str().unwrap(),
            self.bucket_region,
            self.bucket_name,
            self.bucket_prefix,
            self.bucket_files_storage_class,
            self.bucket_files_acl,
            self.sftp_last_mtime,
            self.sftp_filter_regex,
            self.sftp_get_option,
            self.sftp_diff_option,
            self.sftp_download_mode,
            self.local_folder_path,
            self.output_json_path,
            self.sftp_n_sessions,
            self.sftp_buffer_size,
        )
    }
}

/// Launch tasks to perform parallel download
fn launch_download_tasks(
    orig_process_info: &ProcessInfo,
    download_rx: crossbeam_channel::Receiver<Message>,
    upload_tx: mpsc::Sender<Message>,
) {
    // Launch tasks to perform parallel download
    let n_tasks = orig_process_info.sftp_n_sessions;

    for n in 0..n_tasks {
        let process_info = orig_process_info.clone();
        let download_rx = download_rx.clone();
        let upload_tx_for_task = upload_tx.clone();
        let sftp_download_mode = orig_process_info.sftp_download_mode.clone();
        let sftp_buffer_size = orig_process_info.sftp_buffer_size;
        let sftp_config = SftpConfig::new_from_env();

        tokio::spawn(async move {
            let sftp_session = match sftp_download_mode {
                sftp::SftpDownloadMode::AsyncSftp => sftp_config.get_async_sftp_connection().await,
                _ => sftp_config.get_sftp_connection(),
            };

            info!("Task {} consumption starting", n);
            loop {
                let received_msg = download_rx.recv();
                match received_msg {
                    Ok(Message {
                        sftp_path,
                        file_size_in_mb,
                        ..
                    }) => {
                        debug!("Task {} downloading {:?}", n, sftp_path);
                        let download_result = match sftp_session {
                            SftpSession::SyncSftpSession(ref task_ssh_sess, _) => {
                                match sftp_download_mode {
                                    sftp::SftpDownloadMode::Scp => {
                                        download_file_from_scp(
                                            &sftp_path,
                                            task_ssh_sess,
                                            process_info.get_local_folder_path(),
                                            sftp_buffer_size,
                                        )
                                        .await
                                    }
                                    _ => {
                                        let sftp_sess = sftp_session.get_sftp_session().unwrap();
                                        download_file_from_sftp(
                                            &sftp_path,
                                            sftp_sess,
                                            process_info.get_local_folder_path(),
                                            sftp_buffer_size,
                                        )
                                        .await
                                    }
                                }
                            }
                            SftpSession::AsyncSftpSession(_, ref task_sftp_sess) => {
                                async_download_file_from_sftp(
                                    &sftp_path,
                                    task_sftp_sess,
                                    process_info.get_local_folder_path(),
                                )
                                .await
                            }
                        };

                        let (local_path, process_error, success) = match download_result {
                            Err(download_error) => {
                                error!("{}", &download_error.message());
                                (None, Some(ProcessError::SftpError(download_error)), false)
                            }
                            Ok(local_path) => {
                                debug!(
                                    "Task {} downloaded file {}",
                                    n,
                                    &sftp_path.to_str().unwrap()
                                );
                                (Some(local_path), None, true)
                            }
                        };
                        let msg = communication::Message {
                            sftp_path,
                            file_size_in_mb,
                            local_path,
                            n_task: Some(n),
                            process_error,
                            success,
                        };
                        upload_tx_for_task.send(msg).unwrap();
                        sleep(Duration::from_millis(1000)).await;
                    }
                    _ => {
                        info!("Task {} consumption stopped", n);
                        break;
                    }
                }
            }
            info!("Task {} exiting", n);
        });
    }
}

/// Upload loop that launches upload tasks
fn launch_upload_tasks<'a>(
    orig_process_info: &'a ProcessInfo,
    s3_client: &S3Client,
    upload_rx: mpsc::Receiver<Message>,
    upload_result_tx: mpsc::Sender<Message>,
    progress_bar: &'a mut LogProgressBar,
) -> (HashMap<u64, u64>, Vec<JoinHandle<()>>) {
    let mut task_count: HashMap<u64, u64> = HashMap::new();
    let mut handles: Vec<JoinHandle<()>> = vec![];

    'upload_loop: loop {
        let received_msg = upload_rx.recv();
        match received_msg {
            Ok(Message {
                sftp_path,
                local_path: Some(local_path),
                file_size_in_mb,
                n_task: Some(n_task),
                process_error: None,
                ..
            }) => {
                let upload_result_tx_for_task = upload_result_tx.clone();
                let process_info = orig_process_info.clone();
                let aux_task_count = task_count.entry(n_task).or_insert(0);
                *aux_task_count += 1;

                let s3_key: String = sftp::build_s3_key(&sftp_path, &process_info.bucket_prefix);
                debug!(
                    "Uploading local path {} to s3://{}/{}",
                    local_path.to_str().unwrap(),
                    process_info.bucket_name,
                    s3_key
                );
                let s3_client = s3_client.clone();
                let upload_task = tokio::spawn(async move {
                    let upload_result = upload_object_to_s3(
                        &s3_client,
                        &process_info.bucket_name,
                        local_path.as_path(),
                        &s3_key,
                        Some(process_info.bucket_files_acl),
                        Some(process_info.bucket_files_storage_class),
                    )
                    .await;
                    delete_local_path(&local_path).await;
                    let (upload_success, upload_err) = match upload_result {
                        Err(err) => (false, Some(ProcessError::S3UploadError(err))),
                        Ok(_) => (true, None),
                    };
                    let msg = communication::Message {
                        sftp_path,
                        file_size_in_mb,
                        local_path: Some(local_path),
                        n_task: Some(n_task),
                        process_error: upload_err,
                        success: upload_success,
                    };

                    upload_result_tx_for_task.send(msg).unwrap();
                });
                handles.push(upload_task);
                progress_bar.inc();
            }
            Ok(msg) => {
                upload_result_tx.send(msg).unwrap();
                progress_bar.inc();
            }
            _ => break 'upload_loop,
        }
    }
    (task_count, handles)
}

pub async fn run() {
    let process_info = ProcessInfo::new();
    info!("Process information is {}", process_info);
    process_info.set_auxiliar_local_folder().await;

    info!("Initializing SFTP to S3 load");
    let s3_client = get_aws_s3_client(Some(process_info.bucket_region.clone())).await;

    let sftp_config = SftpConfig::new_from_env();
    let sftp_sess: ssh2::Sftp;
    if let SftpSession::SyncSftpSession(_, intern_sftp_sess) = sftp_config.get_sftp_connection() {
        sftp_sess = intern_sftp_sess;
    } else {
        panic!()
    };

    let sftp_source_path = process_info.get_sftp_source_path();
    let mut path_list = list_sftp_dir_and_filter(
        sftp_source_path,
        &sftp_sess,
        process_info.sftp_last_mtime,
        &process_info.sftp_filter_regex,
    );

    path_list = match &process_info.sftp_diff_option {
        sftp::DiffKind::NoDiff => path_list,
        _ => {
            diff_load::filter_diff(
                &path_list,
                &process_info.bucket_name,
                &process_info.bucket_prefix,
                &s3_client,
                &process_info.sftp_diff_option,
            )
            .await
        }
    };

    let path_list_len: u64 = u64::try_from(path_list.len()).unwrap();

    let (download_tx, download_rx) = crossbeam_channel::unbounded::<Message>();
    let (upload_tx, upload_rx) = mpsc::channel::<Message>();
    let (upload_result_tx, upload_result_rx) = mpsc::channel::<Message>();

    // DOWNLOAD TASK
    info!("Download process started with tasks");
    let start = time::Instant::now();
    let mut progress_bar = LogProgressBar::new(path_list_len.try_into().unwrap());
    tokio::spawn(async move {
        for element in &path_list {
            let aux_sftp_path = element.0.clone().into_boxed_path();
            let file_size_in_mb = element.1.clone().size.unwrap_or(0);
            let msg = Message {
                sftp_path: aux_sftp_path.to_path_buf(),
                file_size_in_mb,
                ..Default::default()
            };
            download_tx.send(msg).unwrap();
        }
    });

    // LAUNCH DOWNLOAD TASKS
    launch_download_tasks(&process_info, download_rx, upload_tx);
    // upload_tx is moved into the previous function so no need to drop

    let (task_count, task_handles) = launch_upload_tasks(
        &process_info,
        &s3_client,
        upload_rx,
        upload_result_tx,
        &mut progress_bar,
    );

    info!("Task download count {:?}", task_count);
    let match_duration = start.elapsed();

    // Give statistics about the process
    let total_path_len = path_list_len as f64;
    let mut n_total_successful_bytes: u64 = 0;
    let mut n_processed_paths: f64 = 0.0;
    let mut n_success_paths: f64 = 0.0;
    let mut n_fail_paths: f64 = 0.0;
    let mut n_deletion_fail_paths: usize = 0;
    let mut failed_paths: BTreeMap<String, String> = BTreeMap::new();
    for msg in upload_result_rx {
        n_processed_paths += 1.0;

        if !msg.success {
            n_fail_paths += 1.0;
            failed_paths.insert(
                msg.sftp_path.display().to_string(),
                msg.process_error.unwrap().message(),
            );
        } else {
            n_success_paths += 1.0;
            n_total_successful_bytes += msg.file_size_in_mb;
            if let SftpGetOption::Delete = process_info.sftp_get_option {
                let deletion_result = delete_sftp_path(&msg.sftp_path, &sftp_sess);
                if let Err(error_msg) = deletion_result {
                    error!(
                        "Error while deleting file {:?} with msg {:?}",
                        &msg.sftp_path, error_msg
                    );
                    n_deletion_fail_paths += 1;
                };
            };
        }
    }
    join_all(task_handles).await;
    let n_total_successful_megabytes = n_total_successful_bytes as f64 / 1048576.0;
    let upload_throughput = n_total_successful_megabytes / match_duration.as_secs_f64();

    info!(
        "Final time elapsed for downloading is: {:?}, total processed files are {} \
        and upload throughput is {:.2} MegaBytes/second",
        match_duration, n_processed_paths, upload_throughput
    );
    if total_path_len != n_processed_paths {
        error!(
            "Number of processed paths {} is not equal to the number of target paths {}",
            n_processed_paths, total_path_len
        );
    }
    let success_rate = n_success_paths / total_path_len;
    info!("Success rate of upload is {}", success_rate);

    process_info.delete_auxiliar_local_folder().await;

    let results = json!({
        "n_sftp_ingest_paths": path_list_len,
        "n_processed_paths": n_processed_paths as u64,
        "n_total_successful_megabytes": n_total_successful_megabytes as u64,
        "n_fail_paths": n_fail_paths as u64,
        "n_deletion_fail_paths": n_deletion_fail_paths as u64,
        "success_rate": success_rate,
        "duration_in_seconds": match_duration.as_secs(),
        "failed_sftp_paths": failed_paths,
    });
    info!(
        "Writting output JSON {:?} at {:?}",
        results.to_string(),
        process_info.get_output_json_path()
    );
    fs::write(process_info.get_output_json_path(), results.to_string())
        .expect("Unable to write output JSON with results");

    if success_rate < 1.0 {
        panic!("Output as fail because success rate is < 1.0");
    }
}
