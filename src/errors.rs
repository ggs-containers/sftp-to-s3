use aws_sdk_s3::operation::put_object::PutObjectError;
use std::error;
use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::path::PathBuf;

// SFTP DOWNLOAD ERROR
/// Sftp download file possible errors
#[derive(Clone)]
pub enum SftpDownloadError {
    CantOpen(PathBuf, String),
    CantRead(PathBuf, String),
    CantWriteLocally(PathBuf, String),
    BadLocalPath,
}

impl SftpDownloadError {
    pub fn message(&self) -> String {
        match self {
            Self::CantOpen(sftp_path, msg) => format!(
                "Can not open file {} from sftp remote server with message {}",
                sftp_path.display(),
                msg
            ),
            Self::CantRead(sftp_path, msg) => format!(
                "Can not reead after openning the file {} from sftp remote server with message {}",
                sftp_path.display(),
                msg
            ),
            Self::CantWriteLocally(local_path, msg) => {
                format!(
                    "Can not write {} locally with message {}",
                    local_path.display(),
                    msg
                )
            }
            Self::BadLocalPath => "Bad locally formatted path".to_string(),
        }
    }
}

impl Display for SftpDownloadError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for SftpDownloadError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl error::Error for SftpDownloadError {}

// PROCESS ERROR

/// Process defining error of the possible types: S3 put or sftp download
pub enum ProcessError {
    S3UploadError(aws_sdk_s3::error::SdkError<PutObjectError>),
    SftpError(SftpDownloadError),
}

impl ProcessError {
    pub fn message(&self) -> String {
        match self {
            ProcessError::S3UploadError(error) => error.to_string(),
            ProcessError::SftpError(error) => error.message(),
        }
    }
}

impl Display for ProcessError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for ProcessError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl error::Error for ProcessError {}
